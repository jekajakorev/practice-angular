import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import {Params, Router} from '@angular/router';

@Injectable()
export class MapService {
  locationurl = 'http://localhost:8000/api/v1/location/'
  marker: google.maps.Marker = null;
  constructor(private http: Http,
              private router: Router) { }

  getLocations(id: number | string) {
    return this.http.get(`${this.locationurl}getlocation/${+id}`)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
  saveLocation(name: string) {
      if (this.marker == null) {
           return false;
       } else {
           this.http.post(`${this.locationurl}addlocation/`, {
               token: localStorage.getItem('token'),
               name: name,
               lat: this.marker.getPosition().lat(),
               lng: this.marker.getPosition().lng()
           })
               .map((res: Response) => res.json())
               .subscribe( data => {
                   localStorage.setItem('token', data.token);
                   this.marker.setLabel(name);
                   this.marker.setLabel({text: name, color: '#0033cc', fontSize: '20px', fontWeight: 'bold'});
                   this.marker = null;
               }, error => {
                 if (error.status === 498) {
                     console.log('here');
                     localStorage.removeItem('token');
                   localStorage.removeItem('userId');
                   this.router.navigateByUrl('auth');
                 }
               });
       }
  }


}
