import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ElementRef} from '@angular/core';
import {} from '@types/googlemaps';
import {ActivatedRoute, Params, Router, UrlSegment} from '@angular/router';
import {MapService} from './map.service';
type Pos = {lat: number; lng: number};
type Location = {location: {lat: number, lng: number}, name: string}

@Component({
    selector: 'g-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
    @ViewChild('mapCanvas') mapCanvas: ElementRef;

    private pos: Pos = {
            lat: 46.4598865,
            lng: 30.5717048
    };
    private map: any;
    params: Params;
    url: UrlSegment[];
    marker: google.maps.Marker = null;
    constructor(private service: MapService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((pos) => {
                this.pos.lat = pos.coords.latitude;
                this.pos.lng = pos.coords.longitude;
            });
        }
        this.route.params.subscribe((params: Params) => {
            this.params = params;
            console.log(params);
            this.loadLocations(params['id'] || localStorage.getItem('userId'));
        });
        this.route.url.subscribe((url: UrlSegment[]) => {
            this.url = url;
            console.log(url);
        });

    }

    static _mapCenter(lat: number, lng: number) {
        return new google.maps.LatLng(lat, lng);
    }

    _newplaceMarker(marker: Location) {
        return new google.maps.Marker({
            position: marker.location,
            label: {text: marker.name, color: '#0033cc', fontSize: '20px', fontWeight: 'bold'},
            map: this.map
        });
    }
    ngAfterViewInit() {
        this.map = new google.maps.Map(this.mapCanvas.nativeElement, {
            zoom: 11,
            center: MapComponent._mapCenter(this.pos.lat, this.pos.lng),
            disableDefaultUI: true
        });
        if (this.url[0].path === 'personcab') {
            this.map.addListener('click', (event: google.maps.MouseEvent) => {
                if (this.service.marker == null) {
                    this.service.marker = this._newplaceMarker(
                        {
                            location: {lat: event.latLng.lat(), lng: event.latLng.lng()},
                            name: null
                        });
                    this.service.marker.setAnimation(google.maps.Animation.DROP);
                }else {
                    this.service.marker.setPosition(event.latLng);
                    this.service.marker.setAnimation(google.maps.Animation.DROP);
                }
            });
        }
    }

    private loadLocations(id: string) {
            this.service.getLocations(id).subscribe(data => {
                for (let i = 0; i < data.locations.length; i++) {
                    console.log(data.locations[0]);
                    this._newplaceMarker((
                        {
                            location: {lat: data.locations[i].lat, lng: data.locations[i].lng},
                            name: data.locations[i].name
                        }));
                }
            });
    }
}
