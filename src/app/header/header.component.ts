import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.checkToken();
  }

  getName() {
    return localStorage.getItem('name');
  }
  isLogged() {
    if (localStorage.getItem('token') === null) {
      return false;
    }else {
      return true;
    }
  }
  checkToken(){
    this.authService.checktoken();
  }

}
