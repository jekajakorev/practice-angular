import {Component} from '@angular/core';

@Component({
    template: `
        <h2>Page not found</h2>
        <button routerLink="/">Go to index</button>
    `,
    styleUrls: ['not-found.component.css']
})
export class PageNotFoundComponent {

}
