import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './not-found.component';
import {LoginComponent} from './auth/login/login.component';
import {RegisterComponent} from './auth/register/register.component';
import {PersoncabinetComponent} from './users/personcabinet/personcabinet.component';

const appRoutes: Routes = [
    {path: '', redirectTo: '/users', pathMatch: 'full'},
    {path: 'auth', component: LoginComponent},
    {path: 'auth/register', component: RegisterComponent},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true} // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
