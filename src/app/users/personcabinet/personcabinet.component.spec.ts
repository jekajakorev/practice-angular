import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersoncabinetComponent } from './personcabinet.component';

describe('PersoncabinetComponent', () => {
  let component: PersoncabinetComponent;
  let fixture: ComponentFixture<PersoncabinetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersoncabinetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersoncabinetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
