import { Component, OnInit } from '@angular/core';
import {MapService} from '../../map/map.service';
import {User, UserService} from '../user-list/user.service';
import {Params, Router} from '@angular/router';

@Component({
  selector: 'app-personcabinet',
  templateUrl: './personcabinet.component.html',
  styleUrls: ['./personcabinet.component.css']
})
export class PersoncabinetComponent implements OnInit {
  name = '';
  user: User;
  nameclass: string;
  isEditState: boolean = false;
  constructor(private mapService: MapService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.loadUser();
  }

  locationsave() {
    this.mapService.saveLocation(this.name);
    this.name = '';
  }
  toogleEditState() {
    this.isEditState = !this.isEditState;
  }
  save() {
      this.nameclass = '';
        this.userService.updateUser(this.user)
            .subscribe(data => {
              this.toogleEditState();
              localStorage.setItem('name', this.user.name);
            }, err => {
                if (err.status === 498) {
                    console.log('here');
                    localStorage.removeItem('token');
                    localStorage.removeItem('userId');
                    this.router.navigateByUrl('auth');
                }
                if (err.status === 409) {
                    this.nameclass = 'hatch';
                    console.log('login isnt correct');
                }
            });
  }
  logout(){
      localStorage.removeItem('token');
      localStorage.removeItem('userId');
      localStorage.removeItem('name');
      this.router.navigateByUrl('auth');
  }

  private loadUser() {
    this.userService.getUser(localStorage.getItem('userId'))
        .subscribe(
            user => {
                console.log(user);
                this.user = {name: user.name, occupation: user.occupation, age: user.age, password: ''};
            }, err => {
                if (err.status === 498) {
                    localStorage.removeItem('token');
                    localStorage.removeItem('userId');
                    this.router.navigateByUrl('auth');
                }
                console.log(err);
            });
  }
}
