import 'rxjs/add/operator/switchMap';
import {Component, OnInit, HostBinding} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {User, UserService} from '../user-list/user.service';

@Component({
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
    @HostBinding('style.display') display = 'block';
    isEditState: boolean = false;
    user: User;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: UserService) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.loadUser(params);
        });
    }


    private loadUser(params: Params) {
        this.service.getUser(params['id'])
            .subscribe(
                user => this.user = user,
                err => console.log(err)
            );
    }

}
