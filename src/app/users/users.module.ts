import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserListComponent} from './user-list/user-list.component';
import {UserDetailComponent} from './user-detail/user-detail.component';
import {UserService} from './user-list/user.service';
import {UserRoutingModule} from './user-routing.module';
import {MapComponent} from '../map/map.component';
import {PersoncabinetComponent} from './personcabinet/personcabinet.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        UserRoutingModule
    ],
    declarations: [
        UserListComponent,
        UserDetailComponent,
        PersoncabinetComponent,
        MapComponent
    ],
    providers: [UserService]
})
export class UsersModule {
}
