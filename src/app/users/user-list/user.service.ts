import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {ResponseContentType} from '@angular/http/src/enums';

export class User {
    constructor(public name: string,
                public age: number,
                public occupation: string,
                public password: string) {
    }
}

@Injectable()
export class UserService {
    private userUrl = 'http://localhost:8000/api/v1/';

    constructor(private http: Http) {
    }

    getUsers(name?: string): Observable<User[]> {
        return this.http.get(this.userUrl, {params: {name}})
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

        getUser(id: number | string) {
        return this.http.get(`${this.userUrl}users/${+id}`)
            .map((res: Response) => res.json());
    }

    updateUser(user: User) {
        return this.http.post(`${this.userUrl}updateinfo/`, {
            token: localStorage.getItem('token'),
            name: user.name,
            password: user.password,
            age: user.age,
            occupation: user.occupation
        })
            .map((res: Response) => res.json());
    }
}
