import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {PageNotFoundComponent} from './not-found.component';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {UsersModule} from './users/users.module';
import {HttpModule} from '@angular/http';
import {MapComponent} from './map/map.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HeaderComponent } from './header/header.component';
import {AuthService} from './auth/auth.service';
import {MapService} from './map/map.service';
import { PersoncabinetComponent } from './users/personcabinet/personcabinet.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        UsersModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        LoginComponent,
        RegisterComponent,
        HeaderComponent,
        FooterComponent
    ],
    bootstrap: [AppComponent],
    providers: [AuthService, MapService]
})
export class AppModule {
}
