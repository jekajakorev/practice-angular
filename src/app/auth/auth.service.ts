import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router, UrlSegment} from '@angular/router';

@Injectable()
export class AuthService {
  authurl = 'http://localhost:8000/api/v1/auth/'
  constructor(private http: Http,
              private router: Router,
              private route: ActivatedRoute) { }
  loggin(name: string, password: string) {
    return this.http.post(`${this.authurl}login/`, {name: name, password: password})
          .map((res: Response) => res.json())
  }
  register(name: string, password: string, age: number, occupation: string) {
      return this.http.post(`${this.authurl}register/`, {name: name, password: password, age: age, occupation: occupation})
          .map((res: Response) => res.json());
  }
  checktoken() {
      this.http.post(`${this.authurl}check/`, {token: localStorage.getItem('token')})
          .map((res: Response) => res.json())
          .subscribe(data => {
              localStorage.setItem('token', data.token);
              localStorage.setItem('name', data.name);
              localStorage.setItem('userId', data.id);
          }, error => {
              if ((error.status === 498) || (error.status === 404)) {
                  localStorage.removeItem('token');
                  localStorage.removeItem('name');
                  localStorage.removeItem('userId');
                  this.route.url.subscribe((url: UrlSegment[]) => {
                      if (url[0].path === '/personcab') {
                          this.router.navigateByUrl('');
                      }
                  });
              }
          });
  }
}
