import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    name: '';
    password = '';
    loginclass = 'logininput';
    passclass = 'logininput';
  constructor(private service: AuthService,
              private route: Router) { }

  ngOnInit() {
  }
  loggin() {
      this.loginclass = 'logininput';
      this.passclass = 'logininput';
      this.service.loggin(this.name, this.password).subscribe(data=>{
          localStorage.setItem('token', data.token);
          localStorage.setItem('name', data.name);
          localStorage.setItem('userId', data.id);
          this.route.navigateByUrl('/');
      }, error => {
          if (error.status === 401) {
              this.passclass = 'logininput hatch';
              // Password isn't correct;
          }
          if (error.status === 404) {
              this.loginclass = 'logininput hatch';
              // User isn't found
          }
      });
  }

}
