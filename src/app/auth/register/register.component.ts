import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: string;
  password: string;
  age: number;
  occupation: string;
  loginclass = 'logininput';
  passclass = 'logininput';
  constructor(private service: AuthService,
              private router: Router) { }

  ngOnInit() {
  }
  register() {
    this.loginclass = 'logininput';
    this.passclass = 'logininput';
    if (this.password.length> 3) {
        this.service.register(this.name, this.password, this.age, this.occupation).subscribe(data => {
            localStorage.setItem('token', data.token);
            localStorage.setItem('name', data.name);
            localStorage.setItem('userId', data.id);
            this.router.navigateByUrl('/');
        }, error => {
            if (error.status === 409) {
                this.loginclass = 'logininput hatch';
                console.log('name isnt correct');
            }
        });
    } else {
      this.passclass = 'logininput hatch';
    }
  }
}
